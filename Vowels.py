#Function that counts the number of vowels in a string
def vowels(word):
    numvowels = 0
    for letter in "aeiou":
        numxvowel = 0
        if word.lower().count(letter) > 0:
            numvowels += word.lower().count(letter)
    return numvowels

#The same function using recursion. 
def rvowels(word, position = 0, vowelcount = 0):
    #print(vowelcount)
    word = word.lower()
    if position == len("aeiou"): #Base Case
        #print("Hello World")
        return vowelcount
    vowelcount += word.count('aeiou'[position])
    position += 1
    #print(vowelcount)
    return rvowels(word, position, vowelcount)



print(vowels("Eleanor")) #4
print(vowels("Introduction to Computer Science II")) #14
print()
print(rvowels("Eleanor")) #4
print()
print(rvowels("Introduction to Computer Science II")) #14
